#/bin/bash +x

docker-compose --project-name=${sample-devops-app1} stop &> /dev/null || true &> /dev/null
docker-compose --project-name=${sample-devops-app1} rm --force &> /dev/null || true &> /dev/null
docker stop `docker ps -a -q -f status=exited` &> /dev/null || true &> /dev/null
docker rm -v `docker ps -a -q -f status=exited` &> /dev/null || true &> /dev/null
docker rmi `docker images --filter 'dangling=true' -q --no-trunc` &> /dev/null || true &> /dev/null


docker-compose build

docker-compose --project-name=${sample-devops-app1} up -d
